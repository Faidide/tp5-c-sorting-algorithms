#include "sort.h"
#include <stdio.h>
#include <sys/time.h>
#include <time.h>

// these arrays will be static to avoid running into problems with the stack
uint32_t tableau_a_trier[60000000];
uint32_t tableau_res[60000000];

int cmp_uint32 (void * a, void * b);

// function to get timestamp in ms
uint64_t get_timestamp ();

int main (int argc, char *argv[]) {

  // values to hold time
  uint64_t timestamp, timestamp2;

  printf ("Lancement du programme de test pour sort\n");

  uint32_t tabtest[10] = {12, 5, 12, 1, 2, 4, 7, 43, 15, 150};
  uint32_t tabres[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  // print test tab
  printf("Test tab: { ");
  for(int i=0;i<9;i++) {
    printf("%" PRId32 ", ", tabtest[i]);
  }
  printf("%" PRId32 " };\n", tabtest[9]);

  // sorting array depending on two first bits
  denomsort_r (tabtest, tabres, 10, 3, 0);

  // print test tab
  printf("Test tab: { ");
  for(int i=0;i<9;i++) {
    printf("%" PRId32 ", ", tabres[i]);
  }
  printf("%" PRId32 " };\n", tabres[9]);

  // test des 60000000 nombres de 8 bits
  printf ("Lancement du programme de test pour les 60millions de 8bits\n");
  timestamp = get_timestamp ();
  // on alloue des nombres de 32 bits et on n'en lira que 8 (je ne suis pas sur d'avoir bien compris la consigne mais je vais faire cela)
  denomsort_r(tableau_a_trier, tableau_res, 60000000, 8, 0);
  timestamp2 = get_timestamp ();
  timestamp2 = timestamp2 - timestamp;
  printf("La fonction denomsort a mis %" PRId64 " microsecondes à s'éxécuter sur les 60M\n", timestamp2);
  /* COMMENTÉ POUR LA VITESSE DES TESTS
  // test des 15000000 nombres de 32 bits
  printf ("Lancement du programme de test pour les 15millions de 32bits\n");
  timestamp = get_timestamp ();
  // on alloue des nombres de 32 bits et on n'en lira que 8 (je ne suis pas sur d'avoir bien compris la consigne mais je vais faire cela)
  qsortu_std (tableau_a_trier, 15000000, sizeof(uint32_t), cmp_uint32);
  timestamp2 = get_timestamp ();
  timestamp2 = timestamp2 - timestamp;
  printf("La fonction qsortu a mis %" PRId64 " microsecondes à s'éxécuter sur les 15M\n", timestamp2);
  */ 
  // test des 
  return 0;
}


uint64_t get_timestamp () {
  struct timeval tv;
  gettimeofday (&tv, NULL);
  return tv.tv_sec * (uint64_t)1000000 + tv.tv_usec;
}

int cmp_uint32 (void * a, void * b) { return *(uint32_t*)(a) >= *(uint32_t *)(b); }