#include "sort.h"
#include <stdio.h>

int main (int argc, char *argv[]) {

  printf ("Lancement du programme de test pour sort\n");

  /* TESTING SORTED METHOD */
  uint64_t *tableau = random_array_uint64 (10);
  if (sorted (tableau, 10)) {
    printf ("Tableau 1 trié\n");
  } else {
    printf ("Tableau 1 non trié\n");
  }

  uint64_t tableau2[5] = {1, 2, 3, 4, 5};
  if (sorted (tableau2, 5)) {
    printf ("Tableau 2 trié\n");
  } else {
    printf ("Tableau 2 non trié\n");
  }

  /* TESTING SWAP FUNCTION */
  uint64_t tableau21[5] = {2, 1, 3, 5, 4};
  printf ("tableau21 avant swap: ");
  print_array_uint64_t (tableau21, 5);
  swap (tableau21, 0, 1);
  swap (tableau21, 3, 4);
  printf ("tableau21 apres swap: ");
  print_array_uint64_t (tableau21, 5);
  if (sorted (tableau21, 5)) {
    printf ("Tableau 21 trié\n");
  } else {
    printf ("Tableau 21 non trié\n");
  }

  /* TESTING QSORTU METHOD */
  uint64_t *tableau3 = random_array_uint64 (10);
  qsortu (tableau3, 10);
  if (sorted (tableau3, 10)) {
    printf ("Tableau 3 trié\n");
  } else {
    printf ("Tableau 3 non trié\n");
  }

  /* TESTING QSORTU METHOD */
  uint64_t tableau4[10] = {45, 12, 45, 95, 451, 4577, 4541, 4, 236, 1};
  printf ("Tableau4 avant tri: ");
  print_array_uint64_t (tableau4, 10);
  qsortu (tableau4, 10);
  printf ("Tableau4 apres tri: ");
  print_array_uint64_t (tableau4, 10);
  if (sorted (tableau4, 10)) {
    printf ("Tableau 4 trié\n");
  } else {
    printf ("Tableau 4 non trié\n");
  }

  // tests sur des tableau remplis aléatoirement
  for (int i = 1; i < 10; i++) {
    uint64_t *tableau_i = random_array_uint64 (i * 100);
    // printf("Tableau aléatoire de taille %d avant tri: ", i*100);
    // print_array_uint64_t (tableau_i, i*100);
    qsortu (tableau_i, i * 100);
    // printf("Tableau aléatoire de taille %d apres tri: ", i*100);
    // print_array_uint64_t (tableau_i, i*100);
    if (sorted (tableau_i, i * 100)) {
      printf ("Tableau aléatoire de taille %d trié\n", i * 100);
    } else {
      printf ("Tableau aléatoire de taille %d non trié\n", i * 100);
    }
  }

  return 0;
}