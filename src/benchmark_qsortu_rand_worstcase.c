#include "sort.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#define ARRAY_MAX_SIZE 1000000
#define N_STEPS 1000

// function to get timestamp in ms
uint64_t get_timestamp ();

int main (int argc, char *argv[]) {

  printf ("Lancement du programme de benchmark pour qsortu\n");

  // allocate array
  uint64_t *tableau_i = malloc (sizeof (uint64_t) * ARRAY_MAX_SIZE);
  uint64_t *tableau_i_unsorted = malloc (sizeof (uint64_t) * ARRAY_MAX_SIZE);

  // check that allocation went ok
  if (tableau_i == NULL || tableau_i_unsorted == NULL) {
    perror ("malloc");
    exit (0);
  }

  // fill array with worse case values
  for (int64_t i = 0; i < ARRAY_MAX_SIZE; i++)
    tableau_i_unsorted[i] = (uint64_t) (ARRAY_MAX_SIZE - i);

  // define the step size
  int step_size = ARRAY_MAX_SIZE / N_STEPS;

  // open the file to save the results
  FILE *fd = fopen ("results2.csv", "w+");
  // checks for errors
  if (fd == NULL) {
    perror ("fopen");
    exit (0);
  }

  // value to hold time
  uint64_t timestamp, timestamp2;

  // benchmark for each array length
  for (int i = 0; i < N_STEPS - 1; i++) {
    // print step
    printf ("step %d/%d\n", i, N_STEPS - 1);
    // copy decremental values into the array
    for (int64_t j = 0; j < ((i + 1) * step_size); j++) {
      tableau_i[j] = tableau_i_unsorted[j];
    }
    // get the starting time
    timestamp = get_timestamp ();
    // run the sort
    qsortu_rand (tableau_i, ((i + 1) * step_size));
    // get the finish time
    timestamp2 = get_timestamp ();
    // write the result in the csv
    timestamp2 = timestamp2 - timestamp;
    fprintf (fd, "%" PRId64 ",%d\n", timestamp2, ((i + 1) * step_size));
  }

  // useless but still
  fclose (fd);

  // free are useless as well in the context of an exiting programm

  // for each step
  return 0;
}

uint64_t get_timestamp () {
  struct timeval tv;
  gettimeofday (&tv, NULL);
  return tv.tv_sec * (uint64_t)1000000 + tv.tv_usec;
}
