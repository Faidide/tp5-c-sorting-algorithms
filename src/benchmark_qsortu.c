#include "sort.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#define ARRAY_MAX_SIZE 1000000
#define N_STEPS 1000

// function to get timestamp in ms
uint64_t get_timestamp ();

int main (int argc, char *argv[]) {

  printf ("Lancement du programme de benchmark pour qsortu\n");

  // generate a random array
  uint64_t *tableau_i = random_array_uint64 (ARRAY_MAX_SIZE);

  // define the step size
  int step_size = ARRAY_MAX_SIZE / N_STEPS;

  // open the file to save the results
  FILE *fd = fopen ("results.csv", "w+");
  // checks for errors
  if (fd == NULL) {
    perror ("fopen");
    exit (0);
  }

  // value to hold time
  uint64_t timestamp, timestamp2;

  // benchmark for each array length
  for (int i = 0; i < N_STEPS - 1; i++) {
    // randomize the array
    mix_array (tableau_i, ((i)*step_size));
    // get the starting time
    timestamp = get_timestamp ();
    // run the sort
    qsortu (tableau_i, ((i + 1) * step_size));
    // get the finish time
    timestamp2 = get_timestamp ();
    // write the result in the csv
    timestamp2 = timestamp2 - timestamp;
    fprintf (fd, "%" PRId64 ",%d\n", timestamp2, ((i + 1) * step_size));
  }

  // useless but still
  fclose (fd);

  // for each step
  return 0;
}

uint64_t get_timestamp () {
  struct timeval tv;
  gettimeofday (&tv, NULL);
  return tv.tv_sec * (uint64_t)1000000 + tv.tv_usec;
}
