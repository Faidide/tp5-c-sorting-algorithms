#include "sort.h"

#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>

// to avoid frame stack overflow as we will have later recursive calls, we put a
// maximum amount of variables as static to save stack space
// even variables not related to recursive calls will sometimes be here

// buffer to save the address of new arrays before returning it
uint64_t *new_array_addr_buffer;
// variable to avoid calling srand two times
int srand_magic_number;

// part_qsortu static variables
int64_t i_part, n_part;
uint64_t x_part;

// part_std_qsortu static variables
char x_part_std[MAX_SORT_ELEM_SIZE];
size_t i_part_std, n_part_std;
char copy_buffer[MAX_SORT_ELEM_SIZE];

// generate a random array
uint64_t *random_array_uint64 (int64_t Tlen) {

  // if srand was not called before
  if (srand_magic_number != SRAND_MAGIC_NUMBER) {
    // intializes random number generator
    srand ((unsigned)time (NULL));
    // prevent other calls
    srand_magic_number = SRAND_MAGIC_NUMBER;
  }

  // allocate the memory for the array to be returned
  new_array_addr_buffer = malloc (sizeof (uint64_t) * Tlen);

  //  for eaach case
  for (int i = 0; i < Tlen; i++) {
    // assign random variables
    new_array_addr_buffer[i] = rand () % MAX_VARIABLE;
  }

  return new_array_addr_buffer;
}

// tell if our array is sorted
int sorted (uint64_t *T, int64_t Tlen) {
  // for each couple of elements
  for (int i = 0; i < Tlen - 1; i++) {
    // if they are not ordered
    if (T[i] > T[i + 1]) {
      // return false
      return false;
    }
  }
  // if all couples are sorted, return true
  return true;
}

void mix_array (uint64_t *T, int64_t Tlen) {
  for (int i = 0; i < Tlen; i++) {
    swap (T, rand () % Tlen, rand () % Tlen);
  }
}

void mix_array32 (uint32_t *T, int64_t Tlen) {
  for (int i = 0; i < Tlen; i++) {
    swap32 (T, rand () % Tlen, rand () % Tlen);
  }
}

// quicksort of array T
/******************************************
input: T
input: d, f, x = T[f]
output: n, la nouvelle position de x
dans le tableau modifié en place t.q.
T[d..n-1] <= x; T[n+1..f] > x
******************************************/
void qsortu (uint64_t *T, int64_t Tlen) { qsortu_rec (T, 0, Tlen - 1); }

void qsortu_rand (uint64_t *T, int64_t Tlen) {
  qsortu_rand_rec (T, 0, Tlen - 1);
}

void qsortu_rec (uint64_t *T, int64_t lo, int64_t hi) {
  if (lo < hi) {
    // call partition and get the pivot number
    int64_t pivot = part_qsortu (T, lo, hi);

    // recursively call the qsortu method on smaller arrays
    qsortu_rec (T, lo, pivot - 1);
    qsortu_rec (T, pivot + 1, hi);
  }
}

// parition funtion for qsortu (variables are static to keep recursion from
// blowing the frame stack) lomuto partition scheme
int64_t part_qsortu (uint64_t *T, int64_t lo, int64_t hi) {
  // init variables (choose pivot)
  x_part = T[hi];
  n_part = lo;

  // for each item between lo and hi
  for (i_part = lo; i_part <= hi; i_part++) {
    // if current item is higher than our pivot
    if (T[i_part] < x_part) {
      // invert them
      swap (T, n_part, i_part);
      n_part++;
    }
  }
  // finally, put the pivot back in the middle
  swap (T, n_part, hi);
  // and return the pivot index
  return n_part;
}

void qsortu_rand_rec (uint64_t *T, int64_t lo, int64_t hi) {
  if (lo < hi) {
    // call partition and get the pivot number
    int64_t pivot = part_rand_qsortu (T, lo, hi);

    // recursively call the qsortu method on smaller arrays
    qsortu_rand_rec (T, lo, pivot - 1);
    qsortu_rand_rec (T, pivot + 1, hi);
  }
}

int64_t part_rand_qsortu (uint64_t *T, int64_t lo, int64_t hi) {
  // swap random variable with last one
  swap (T, hi, lo + (rand () % (hi - lo)));

  // init variables (choose pivot)
  x_part = T[hi];
  n_part = lo;

  // for each item between lo and hi
  for (i_part = lo; i_part <= hi; i_part++) {
    // if current item is higher than our pivot
    if (T[i_part] < x_part) {
      // invert them
      swap (T, n_part, i_part);
      n_part++;
    }
  }
  // finally, put the pivot back in the middle
  swap (T, n_part, hi);
  // and return the pivot index
  return n_part;
}

// swap two elements in array T
void swap (uint64_t *T, int64_t first, int64_t second) {
  // ignore when indexes are the same
  if (first == second)
    return;
  // swap the variables
  T[first] = T[first] + T[second];
  T[second] = T[first] - T[second];
  T[first] = T[first] - T[second];
  return;
}

void swap32 (uint32_t *T, int64_t first, int64_t second) {
  // ignore when indexes are the same
  if (first == second)
    return;
  // swap the variables
  T[first] = T[first] + T[second];
  T[second] = T[first] - T[second];
  T[first] = T[first] - T[second];
  return;
}


// quicksort with similar signature as std's qsort
void qsortu_std (void *base, size_t num, size_t size,
                 int (*compar) (const void *, const void *)) {
  // call the recursive version
  qsortu_std_rec (base, num, size, compar, 0, num - 1);
}

// quicksort with similar signature as std's qsort
void qsortu_std_rec (void *base, size_t num, size_t size,
                     int (*compar) (const void *, const void *), size_t lo,
                     size_t hi) {
  if (lo < hi) {
    // call partition and get the pivot number
    size_t pivot = part_std_qsortu (base, size, compar, lo, hi);

    // recursively call the qsortu method on smaller arrays
    if (pivot != 0) {
      qsortu_std_rec (base, num, size, compar, lo, pivot - (size_t)1);
    }
    qsortu_std_rec (base, num, size, compar, pivot + (size_t)1, hi);
  }
}

// quicksort partitionning with similar signature as std's qsort
size_t part_std_qsortu (void *base, size_t size,
                        int (*compar) (const void *, const void *), size_t lo,
                        size_t hi) {

  // init variables (choose pivot)
  memcpy (x_part_std, base + (hi * size), size);
  n_part_std = lo;

  // for each item between lo and hi
  for (i_part_std = lo; i_part_std <= hi; i_part_std++) {
    // if current item is higher than our pivot
    if (compar ((void *)(base + (size * (i_part_std))), (void *)(x_part_std)) ==
        false) {
      // invert them
      swap_std (base, size, n_part_std, i_part_std);
      n_part_std = n_part_std + 1;
    }
  }
  // finally, put the pivot back in the middle
  swap_std (base, size, n_part_std, hi);
  // and return the pivot index
  return n_part_std;
}

// function to swap array of undefined size
void swap_std (void *base, size_t size, size_t first, size_t second) {

  if (first == second) {
    return;
  }

  // move first to buffer
  for (int i = 0; i < size; i++) {
    copy_buffer[i] = *(char *)(base + (first * size) + i);
  }
  // move second to first
  for (int i = 0; i < size; i++) {
    *(char *)(base + (first * size) + i) =
        *(char *)(base + (second * size) + i);
  }
  // move buffer to second
  for (int i = 0; i < size; i++) {
    *(char *)(base + (second * size) + i) = copy_buffer[i];
  }
}

// a char based version of memove
void memove_char (void *dest, void *src) { *(char *)(dest) = *(char *)(src); }

// static variables for denomsort_r
uint32_t number_count_den[MAX_DENOM_SIZE];
uint32_t number_count2_den[MAX_DENOM_SIZE];
uint32_t number_count_cumsum[MAX_DENOM_SIZE];
unsigned int mask_den;
uint64_t item_count_den;

//  denominator sort for 32bits integers
uint32_t *denomsort_r (const uint32_t *T, uint32_t *Ts, int64_t Tlen,
                       uint64_t width, uint64_t pos) {

  // init indexes & buffers
  item_count_den=0;

  // init count tab with 0s
  for (uint64_t i = 0; i < (pow (2, width)); i++) {
    number_count_den[i] = 0;
    number_count2_den[i] = 0;
    number_count_cumsum[i] = 0;
  }

  // we will use the following mask
  mask_den = (1 << width) - 1;

  // count variables
  for (uint64_t i = 0; i < Tlen; i++) {
    // we shift the value pos*width times to the beginning and erase other
    // values with bitwise and using the mask
    number_count_den[((T[i] >> (pos * width)) & (mask_den))]++;
  }

  // compute the cumulated sum of indexing array
  for (uint64_t i=0 ; i< pow(2,width);i++) {
    number_count_cumsum[i]=item_count_den;
    item_count_den += number_count_den[i];
  }

  // save variables
  for (uint64_t i = 0; i < Tlen; i++) {
    // insert the variable at its final position
    Ts[ number_count_cumsum[((T[i] >> (pos * width)) & (mask_den))] + number_count2_den[((T[i] >> (pos * width)) & (mask_den))] ] = T[i];
    number_count2_den[((T[i] >> (pos * width)) & (mask_den))]++;
  }

  return Ts;
}

// radixsort of array T
/******************************************
input: nlen, width
input: T, un tableau de nombres dont les
valeurs peuvent être représentées avec
nlen*width bits
output: Ts, qui contient les éléments de T
triés
******************************************/
void radixsortu (uint32_t *T, uint32_t *Ts, int64_t Tlen, uint64_t width, uint64_t nlen) {
  uint32_t * buffer_ptr;
  uint32_t * T_rad;
  uint32_t * Ts_rad;

  T_rad = T;
  Ts_rad = Ts;

  for(int i=0;i<nlen;i++) {
    denomsort_r (T_rad, Ts_rad, Tlen, width, i);
    // swap array
    buffer_ptr = T_rad;
    T_rad = Ts_rad;
    Ts_rad = buffer_ptr;
  }
}

// print the array
void print_array_uint64_t (uint64_t *T, int64_t Tlen) {
  printf ("{");
  for (int i = 0; i < Tlen - 1; i++) {
    printf ("%" PRId64 ",", T[i]);
  }
  printf ("%" PRId64 "}\n", T[Tlen - 1]);
}

void print_7b_string_array (void *target, size_t size) {
  for (int i = 0; i < size; i++) {
    printf ("element %d: ", i);
    for (int j = 0; j < 7; j++) {
      printf ("%c", *(char *)(target + (i * 7) + j));
    }
    printf ("\n");
  }
}