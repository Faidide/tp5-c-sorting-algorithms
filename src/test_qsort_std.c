#include "sort.h"
#include <stdio.h>

int is_7b_string_smaller ( void * first, void * second );

int main (int argc, char *argv[]) {

  // test is_7b_string_smaller
  char first[7] = "azerty";
  char second[7] = "qwerty";

  if( is_7b_string_smaller ((void *)first, (void *)second) == true) {
    printf("first is bigger or equal\n");
  } else {
    printf("first is smaller\n");
  }

  // now let's get serious, we will test our homemade sorting function
  char bins_array[35] = "jhfduylkjhgfdertyhiokhgfdsertgfred";
  bins_array[34]='f';
  printf("Initial array:\n");
  print_7b_string_array (bins_array, 5);

  qsortu_std (bins_array, 5, 7, is_7b_string_smaller) ;

  printf("Final array:\n");
  print_7b_string_array (bins_array, 5);

  return 0;
}

int is_7b_string_smaller ( void * first, void * second ) {
  // for each bytechar
  for(int i=0;i<7;i++) {
    // if first is bigger than second at this rank
    if(*(char*)(first+i) > *(char*)(second+i)) {
      // return true (sorted)
      return true;
    // if first is smalle rtha nsecond at this rank
    } else if (*(char*)(first+i) < *(char*)(second+i)) {
      // it's not sorted
      return false;
    }
    // if equal, continue
  }
  // if they are all equal, return true
  // return true (sorted)
  return true;
}

