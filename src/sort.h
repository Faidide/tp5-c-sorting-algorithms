#ifndef DEF_SORT_H
#define DEF_SORT_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#define SRAND_MAGIC_NUMBER 124578956

#define MAX_VARIABLE 10000

#define MAX_SORT_ELEM_SIZE 100

#define MAX_DENOM_SIZE 1000000

#define RADIXSORT_GROUPY_BITS 2

// generate a random array
uint64_t *random_array_uint64 (int64_t Tlen);

// mix an already existing array
void mix_array (uint64_t *T, int64_t Tlen);
void mix_array32 (uint32_t *T, int64_t Tlen);

// tell if our array is sorted
int sorted (uint64_t *T, int64_t Tlen);

// quicksort of array T
void qsortu (uint64_t *T, int64_t Tlen);
void qsortu_rec (uint64_t *T, int64_t lo, int64_t hi);
int64_t part_qsortu (uint64_t *T, int64_t lo, int64_t hi);

// quicksort with random pivot
void qsortu_rand (uint64_t *T, int64_t Tlen);
void qsortu_rand_rec (uint64_t *T, int64_t lo, int64_t hi);
int64_t part_rand_qsortu (uint64_t *T, int64_t lo, int64_t hi);

// quicksort with similar signature as std's qsort
void qsortu_std (void *base, size_t num, size_t size,
                 int (*compar) (const void *, const void *));

void qsortu_std_rec (void *base, size_t num, size_t size,
                     int (*compar) (const void *, const void *), size_t lo,
                     size_t hi);

size_t part_std_qsortu (void *base, size_t size,
                         int (*compar) (const void *, const void *), size_t lo,
                         size_t hi);
// function to swap array of undefined size
void swap_std (void *base, size_t size, size_t lo, size_t hi);

// swap two items in an array
void swap (uint64_t *T, int64_t first, int64_t second);
void swap32 (uint32_t *T, int64_t first, int64_t second);

//  denominator sort for 32bits integers
uint32_t *denomsort_r (const uint32_t *T, uint32_t *Ts, int64_t Tlen,
                       uint64_t width, uint64_t pos);

// radixsort of array T
void radixsortu (uint32_t *T, uint32_t *Ts, int64_t Tlen, uint64_t width, uint64_t nlen);

// print the array
void print_array_uint64_t (uint64_t *T, int64_t Tlen);

// char based memove
void memove_char (void *dest, void *src);

void print_7b_string_array ( void * target, size_t size );

#endif // DEF_SORT_H