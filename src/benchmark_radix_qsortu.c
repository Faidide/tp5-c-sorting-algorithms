#include "sort.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#define ARRAY_MAX_SIZE 100000
#define N_STEPS 300

// function to get timestamp in ms
uint64_t get_timestamp ();

int cmp_uint32 (void * a, void * b);

int main (int argc, char *argv[]) {

  printf ("Lancement du programme de benchmark pour qsortu\n");

  // generate a random array
  // we'll make the random generation believe it's generation uint64 array x)
  uint32_t *tableau_i = (uint32_t*)(random_array_uint64 (ARRAY_MAX_SIZE));
  uint32_t *tableau_i2 = (uint32_t*)(random_array_uint64 (ARRAY_MAX_SIZE));

  // define the step size
  int step_size = ARRAY_MAX_SIZE / N_STEPS;

  // open the file to save the results
  FILE *fd = fopen ("results_rad.csv", "w+");
  // checks for errors
  if (fd == NULL) {
    perror ("fopen");
    exit (0);
  }

  // value to hold time
  uint64_t timestamp, timestamp2;

  // benchmark for each array length
  for (int i = 0; i < N_STEPS - 1; i++) {
    // randomize the array
    mix_array32 (tableau_i, ((i)*step_size));
    // get the starting time
    timestamp = get_timestamp ();
    // run the sort
    qsortu_std (tableau_i, ((i + 1) * step_size), sizeof(uint64_t), cmp_uint32);
    // get the finish time
    timestamp2 = get_timestamp ();
    // write the result in the csv
    timestamp2 = timestamp2 - timestamp;
    fprintf (fd, "%" PRId64 ",%d,", timestamp2, ((i + 1) * step_size));

    // randomize the array
    mix_array32 (tableau_i, ((i)*step_size));
    // get the starting time
    timestamp = get_timestamp ();
    // run the sort
    radixsortu (tableau_i, tableau_i2, ((i + 1) * step_size), 16, 2);
    // get the finish time
    timestamp2 = get_timestamp ();
    // write the result in the csv
    timestamp2 = timestamp2 - timestamp;
    fprintf (fd, "%" PRId64 "\n", timestamp2);
  }

  // useless but still
  fclose (fd);

  // for each step
  return 0;
}

uint64_t get_timestamp () {
  struct timeval tv;
  gettimeofday (&tv, NULL);
  return tv.tv_sec * (uint64_t)1000000 + tv.tv_usec;
}

int cmp_uint32 (void * a, void * b) { return *(uint32_t*)(a) >= *(uint32_t *)(b); }
