# TP5 C Algorithmes de Tri 
Travaux pratique de programmation en C numéro 5.

## Construire les binaires et les éxécuter
```bash
cd build
make
./bin/test_sort
```

## Accéder au compte rendu
Le compte rendu est le fichier:
```
doc/rapport.pdf
```

## À quoi servent les scripts python
Les scripts python sont utilisés pour générer les graph à partir des csv que les binaires nous sortent, et à une occasion à faire une régression linéaire.

