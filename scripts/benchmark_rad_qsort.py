import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# read the data
bench_data = pd.read_csv("../res/results_rad.csv", header=None)

# plot it
y = bench_data[0];
y2 = bench_data[2];
x = bench_data[1];

plt.plot(x,y, "r", label="qsortu runtime");
plt.plot(x,y2, "g", label="radixsortu runtime");
plt.grid();
plt.title("Temps de calcul de qsortu et radixsortu")
plt.legend();
plt.savefig("output.jpg");
