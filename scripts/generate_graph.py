import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# read the data
bench_data = pd.read_csv("../res/benchmark_qsortu.csv", header=None)

# plot it
y = bench_data[0];
x = bench_data[1];

# compute a linear regression
X_b = np.c_[np.ones((999,1)),x]
theta_best = np.linalg.inv(X_b.T.dot(X_b)).dot(X_b.T).dot(y)
# sample the line
X_new = np.array([[0], [999000]])
X_new_b = np.c_[np.ones((2,1)), X_new]
y_predicted = X_new_b.dot(theta_best)


plt.plot(X_new, y_predicted, "b-", label="linear regression")
plt.plot(x,y, "r", label="qsortu runtime");
plt.grid();
plt.title("Temps de calcul de qsortu versus sa régression linéaire")
plt.legend();
plt.savefig("output.jpg");
