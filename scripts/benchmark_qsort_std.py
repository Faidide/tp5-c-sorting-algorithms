import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# read the data
bench_data = pd.read_csv("../res/benchmark_qsort_std.csv", header=None)

# plot it
y = bench_data[0];
y2 = bench_data[2];
x = bench_data[1];

plt.plot(x,y, "r", label="qsortu_std fait maison");
plt.plot(x,y2, "g", label="qsort de la libraire standard");
plt.grid();
plt.title("Temps de calcul de qsortu_std et qsort")
plt.legend();
plt.savefig("output.jpg");
