import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# read the data
bench_data = pd.read_csv("../res/benchmark_qsortu.csv", header=None)
# read the data
bench_data2 = pd.read_csv("../res/benchmark_qsortu_worst.csv", header=None)

# plot it
y = bench_data[0];
y2 = bench_data2[0];
x = bench_data[1];
x2 = bench_data2[1];

plt.plot(x,y, "r", label="qsortu runtime moyen");
plt.plot(x2,y2, "g", label="qsortu runtime worst case");
plt.xlim((0,10000))
plt.grid();
plt.title("Temps de calcul de qsortu")
plt.legend();
plt.savefig("output.jpg");
