#!/bin/sh

echo -e "\033[33m========== \033[33m Execution des tests pour le paquetage \033[35m test_denomsort \033[33m ===========\033[0m"
printf "%-30.30s" "Execution des tests..."
./build/bin/test_denomsort > test/test_denomsort.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test/test_denomsort.output test/test_denomsort.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec non fatal\033[0m"

fi

echo -e "\033[33m========== \033[33m Execution des tests pour le paquetage \033[35m test_qsort_std \033[33m ===========\033[0m"
printf "%-30.30s" "Execution des tests..."
./build/bin/test_qsort_std > test/test_qsort_std.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test/test_qsort_std.output test/test_qsort_std.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

echo -e "\033[33m========== \033[33m Execution des tests pour le paquetage \033[35m test_radixsort \033[33m ===========\033[0m"
printf "%-30.30s" "Execution des tests..."
./build/bin/test_radixsort > test/test_radixsort.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test/test_radixsort.output test/test_radixsort.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec non fatal\033[0m"
fi

echo -e "\033[33m========== \033[33m Execution des tests pour le paquetage \033[35m test_sort \033[33m ===========\033[0m"
printf "%-30.30s" "Execution des tests..."
./build/bin/test_sort > test/test_sort.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test/test_sort.output test/test_sort.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec non fatal\033[0m"
fi


echo " "
echo -e "\033[32mTous les tests on été passés sans erreur fatale!\033[0m"
